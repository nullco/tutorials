import unittest
from two_sum_bst import TreeNode, Solution

class TestTwoSumCase(unittest.TestCase):

    def test_two_sum_bst(self):
        root = TreeNode(5)
        root.left = TreeNode(3)
        root.right = TreeNode(6)
        root.left.left = TreeNode(2)
        root.left.right = TreeNode(4)
        root.right.right = TreeNode(7)

        self.assertEqual(Solution().findTarget(root, 9), True)

