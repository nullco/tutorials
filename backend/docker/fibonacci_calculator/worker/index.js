const keys = require('./keys');
const redis = require('redis');

const redisClient = redis.createClient({
    host: keys.redisHost,
    port: keys.redisPort,
    retry_strategy: () => 1000
});

// Client with connection in subscriber mode cannot send normal commands to redis
// That's why we need a separate client
const sub = redisClient.duplicate();

function fib(index) {
    if (index < 2) return 1;
    return fib(index - 1) + fib(index - 2);
}

// When an insert message comes, compute fib and set the result in redis
sub.on('message', (channel, message) => {
    redisClient.hset('values', message, fib(parseInt(message)))
});
sub.subscribe('insert');