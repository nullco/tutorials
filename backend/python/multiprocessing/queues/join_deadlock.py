import time
from multiprocessing import Process, Queue

def f(q):
    # The object to be sent is too large, and has to be buffered
    # The process cannot end until the feeder gets all from the buffer
    q.put('X' * 1000000)
    q.put('X' * 1000000)
    q.put('X' * 1000000)

if __name__ == '__main__':
    queue = Queue()
    p = Process(target=f, args=(queue,))
    p.start()
    p.join() # This deadlocks, the main process blocks until the child completes, but no elements are read from the queue
    # to avoid it just comment the previous line
    obj = queue.get()