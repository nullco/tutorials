"""
Concurrent futures was introduced in Python 3.2
Encapsulates process and thread operations in a minimalistic interface
This shows an example of usage with processes by using a ProcessPoolExecutor
"""
import time
import os
import concurrent.futures
from multiprocessing import current_process

start = time.perf_counter()

def do_something(seconds):
    pname = current_process().name
    print(f'{pname}: Sleeping {seconds} second(s)...')
    # sleep is not CPU related
    time.sleep(seconds)
    return pname, seconds

def process_futures(executor):
    """ Manipulates futures directly
    """
    # create futures
    results = [executor.submit(do_something, sec) for sec in [5, 4, 3, 2, 1]]
    # yields futures that are completed.
    for future in concurrent.futures.as_completed(results):
        pname, seconds = future.result()
        print(f'{pname}: Finished in {seconds} second(s)')

def process_results(executor):
    """ Manipulates results instead of futures
    The map operation processes in parallel the args and returns them in the order they where inserted.
    """
    # The results are already the values returned
    results = executor.map(do_something, [5, 4, 3, 2, 1])
    for pname, seconds in results:
        print(f'{pname}: Finished in {seconds} second(s)')

with concurrent.futures.ProcessPoolExecutor() as executor:
    # process_futures(executor)
    process_results(executor)

finish = time.perf_counter()

print(f'All Finished in {round(finish - start, 2)} second(s)')