def binary_search(input_array, value):
    start = 0
    end = len(input_array) - 1
    while start <= end:
        # this will give the middle-lower index in even lengths 
        # and the middle in odd lenghts
        middle = start + (end - start) / 2
        if value > input_array[middle]:
            start = middle + 1
        elif value < input_array[middle]:
            end = middle - 1
        else:
            return middle
    return -1