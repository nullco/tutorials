# f(5) = 5 * 4 * 3 * 2 * 1
# f(4) = 4 * 3 * 2 * 1
# f(5) = 5 * f(4)

def factorial(n):
    if n > 1:
        return n * factorial(n - 1)
    else :
        return 1
    