import multiprocessing
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='(%(processName)-9s) %(message)s',)
                    
def wait_for_event(e):
    logging.debug('wait_for_event starting')
    # blocks until Event.set() is called in the producer
    event_is_set = e.wait()
    logging.debug('event set: %s', event_is_set)

def wait_for_event_timeout(e, t):
    while not e.is_set():
        logging.debug('wait_for_event_timeout starting')
        # blocks until timeout or Event.set() is called in the producer
        event_is_set = e.wait(t)
        logging.debug('event set: %s', event_is_set)
        if event_is_set:
            # event triggered
            logging.debug('processing event')
        else:
            # means timeout exceeded and processes other stuff
            logging.debug('doing other things')

def event_producer(e):
    time.sleep(3)
    e.set()
    logging.debug('Event is set')

if __name__ == '__main__':
    e = multiprocessing.Event()
    p1 = multiprocessing.Process(name='blocking', 
                      target=wait_for_event,
                      args=(e,))
    p1.start()

    p2 = multiprocessing.Process(name='non-blocking', 
                      target=wait_for_event_timeout, 
                      args=(e, 2))
    p2.start()

    logging.debug('Waiting before calling Event.set()')

    p3 = multiprocessing.Process(name='producer', 
                      target=event_producer, 
                      args=(e,))
    p3.start()