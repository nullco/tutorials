import unittest
from graph import Graph

class TestGraphCase(unittest.TestCase):

    def setUp(self):
        graph = Graph()

        # You do not need to change anything below this line.
        # You only need to implement Graph.dfs_helper and Graph.bfs

        graph.set_node_names(('Mountain View',   # 0
                            'San Francisco',   # 1
                            'London',          # 2
                            'Shanghai',        # 3
                            'Berlin',          # 4
                            'Sao Paolo',       # 5
                            'Bangalore'))      # 6 

        graph.insert_edge(51, 0, 1)     # MV <-> SF
        graph.insert_edge(51, 1, 0)     # SF <-> MV
        graph.insert_edge(9950, 0, 3)   # MV <-> Shanghai
        graph.insert_edge(9950, 3, 0)   # Shanghai <-> MV
        graph.insert_edge(10375, 0, 5)  # MV <-> Sao Paolo
        graph.insert_edge(10375, 5, 0)  # Sao Paolo <-> MV
        graph.insert_edge(9900, 1, 3)   # SF <-> Shanghai
        graph.insert_edge(9900, 3, 1)   # Shanghai <-> SF
        graph.insert_edge(9130, 1, 4)   # SF <-> Berlin
        graph.insert_edge(9130, 4, 1)   # Berlin <-> SF
        graph.insert_edge(9217, 2, 3)   # London <-> Shanghai
        graph.insert_edge(9217, 3, 2)   # Shanghai <-> London
        graph.insert_edge(932, 2, 4)    # London <-> Berlin
        graph.insert_edge(932, 4, 2)    # Berlin <-> London
        graph.insert_edge(9471, 2, 5)   # London <-> Sao Paolo
        graph.insert_edge(9471, 5, 2)   # Sao Paolo <-> London
        # (6) 'Bangalore' is intentionally disconnected (no edges)
        # for this problem and should produce None in the
        # Adjacency List, etc.
        self.graph = graph


    def test_get_edge_list_names(self):
        self.assertEqual(self.graph.get_edge_list_names(),
            [ 
                (51, 'Mountain View', 'San Francisco'),
                (51, 'San Francisco', 'Mountain View'),
                (9950, 'Mountain View', 'Shanghai'),
                (9950, 'Shanghai', 'Mountain View'),
                (10375, 'Mountain View', 'Sao Paolo'),
                (10375, 'Sao Paolo', 'Mountain View'),
                (9900, 'San Francisco', 'Shanghai'),
                (9900, 'Shanghai', 'San Francisco'),
                (9130, 'San Francisco', 'Berlin'),
                (9130, 'Berlin', 'San Francisco'),
                (9217, 'London', 'Shanghai'),
                (9217, 'Shanghai', 'London'),
                (932, 'London', 'Berlin'),
                (932, 'Berlin', 'London'),
                (9471, 'London', 'Sao Paolo'),
                (9471, 'Sao Paolo', 'London')
            ]
        )

    def test_get_adjacency_list_names(self):
        self.assertEqual(self.graph.get_adjacency_list_names(), 
            [ 
                [('San Francisco', 51), ('Shanghai', 9950), ('Sao Paolo', 10375)],
                [('Mountain View', 51), ('Shanghai', 9900), ('Berlin', 9130)],
                [('Shanghai', 9217), ('Berlin', 932), ('Sao Paolo', 9471)],
                [('Mountain View', 9950), ('San Francisco', 9900), ('London', 9217)],
                [('San Francisco', 9130), ('London', 932)],
                [('Mountain View', 10375), ('London', 9471)],
                None
            ]
        )

    def test_get_adjacency_matrix(self):
        self.assertEqual(self.graph.get_adjacency_matrix(),
            [ 
                [0, 51, 0, 9950, 0, 10375, 0],
                [51, 0, 0, 9900, 9130, 0, 0],
                [0, 0, 0, 9217, 932, 9471, 0],
                [9950, 9900, 9217, 0, 0, 0, 0],
                [0, 9130, 932, 0, 0, 0, 0],
                [10375, 0, 9471, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0]
            ]
        )

    def test_dfs_names(self):
        self.assertEqual(self.graph.dfs_names(2), 
            ['London', 'Shanghai', 'Mountain View', 'San Francisco', 'Berlin', 'Sao Paolo']
        )

    def test_bfs_names(self):
        self.assertEqual(self.graph.bfs_names(2), 
            ['London', 'Shanghai', 'Berlin', 'Sao Paolo', 'Mountain View', 'San Francisco']
        )