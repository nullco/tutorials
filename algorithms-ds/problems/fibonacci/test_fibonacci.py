import unittest
import iterative
import recursive

class TestFibonacciCase(unittest.TestCase):

    def test_iterative_fibonacci(self):
        self.assertEqual(iterative.fibonacci(1), 1)
        self.assertEqual(iterative.fibonacci(2), 1)
        self.assertEqual(iterative.fibonacci(3), 2)
        self.assertEqual(iterative.fibonacci(4), 3)
        self.assertEqual(iterative.fibonacci(5), 5)

    def test_recursive_fibonacci(self):
        self.assertEqual(recursive.fibonacci(1), 1)
        self.assertEqual(recursive.fibonacci(2), 1)
        self.assertEqual(recursive.fibonacci(3), 2)
        self.assertEqual(recursive.fibonacci(4), 3)
        self.assertEqual(recursive.fibonacci(5), 5)