import unittest
from quick_sort import quicksort

class TestSortingCase(unittest.TestCase):

    def test_quick_sort(self):
        test = [21, 4, 1, 3, 9, 20, 25, 6, 21, 14]
        output = [1, 3, 4, 6, 9, 14, 20, 21, 21, 25]
        self.assertEqual(quicksort(test), output)
