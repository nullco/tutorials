""" Queues allow communication across processes (n-producers, n-consumers)
"""

from multiprocessing import Process, Queue

def square(numbers, queue):
    for n in numbers:
        queue.put(n * n)

def cube(numbers, queue):
    for n in numbers:
        queue.put(n * n * n)

if __name__ == "__main__":
    numbers = range(5)
    queue = Queue()

    square_process = Process(target=square, args=(numbers, queue))
    cube_process = Process(target=cube, args=(numbers, queue))

    square_process.start()
    cube_process.start()

    while any([p.is_alive() for p in [square_process, cube_process]]):
        while not queue.empty():
            print(queue.get())