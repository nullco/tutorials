"""  A pool controls worker process to which jobs can be submitted.
Supports async results with timeouts and callbacks. Also support parallel map.
"""
from multiprocessing import Pool

def sum_square(number):
    s = 0
    for i in range(number):
        s += i * i
    return s

if __name__ == "__main__":

    numbers = range(5)

    with Pool() as pool:
        # blocks until the result is ready
        results = pool.map(sum_square, numbers)

        print(results)