import unittest
import iterative

class TestBinarySearch(unittest.TestCase):

    def test_iterative_binary_search(self):
        input_array = [1,3,9,11,15,19,29]
        self.assertEqual(iterative.binary_search(input_array, 25), -1)
        self.assertEqual(iterative.binary_search(input_array, 15), 4)

        input_array = [2, 4]
        self.assertEqual(iterative.binary_search(input_array, 2), 0)
        self.assertEqual(iterative.binary_search(input_array, 4), 1)
        self.assertEqual(iterative.binary_search(input_array, 6), -1)
        
        input_array = []
        self.assertEqual(iterative.binary_search(input_array, 6), -1)

        input_array = [2]
        self.assertEqual(iterative.binary_search(input_array, 2), 0)
        self.assertEqual(iterative.binary_search(input_array, 3), -1)



