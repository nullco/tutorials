""" A heap is a special type of tree with some additional rules.

Elements are arranged in increasing or decreasing order
the root can be either the MAX (max heap) or MIN value (min heap) of the tree

In heaps, parents can have any number of children (not binary)

Efficiency:
    Peek (max or min): O(1)
    Search: O(n)
    Insert: O(log(n))
    Delete: O(log(n))
"""