from odd_occuring_element import Sets, XOR
import unittest

class TestOddOccuringElementCase(unittest.TestCase):

    def test_odd_occurring_element(self):
        arr = [3, 4, 3, 4, 1, 2, 2]
        expected = 1
        self.assertEqual(expected, Sets.odd_ocurring_element(arr))
        self.assertEqual(expected, XOR.odd_ocurring_element(arr))
        
        arr = [124, 24, 5, 24, 124]
        expected = 5
        self.assertEqual(expected, Sets.odd_ocurring_element(arr))
        self.assertEqual(expected, XOR.odd_ocurring_element(arr))
