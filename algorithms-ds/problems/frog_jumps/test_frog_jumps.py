import unittest
from frog_jumps import frog_jumps

class TestFrogInRiverCase(unittest.TestCase):

    def test_frog_in_river(self):
        self.assertEqual(frog_jumps(1), 2)
        self.assertEqual(frog_jumps(2), 3)
        self.assertEqual(frog_jumps(3), 5)
        self.assertEqual(frog_jumps(4), 8)