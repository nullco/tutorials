# What is needed for async:
# - non-blocking sockets
# - callbacks
# - event loop

import socket
from timeit import timeit
from selectors import DefaultSelector, EVENT_WRITE, EVENT_READ

selector = DefaultSelector()
n_jobs = 0

def get(path):
    global n_jobs
    n_jobs += 1
    s = socket.socket()
    # this will make the socket raise errors when the executed methods on it
    # don't return immediately
    s.setblocking(False)
    try:
        # this raises an exception since connect does not return immediately
        s.connect(('httpbin.org', 80))
    except BlockingIOError:
        pass
    # we register a callback as our data that we associate when the socket is writable
    callback = lambda: connected(s, path)
    selector.register(s.fileno(), EVENT_WRITE, callback)

def connected(s, path):
    selector.unregister(s.fileno())
    request = 'GET %s HTTP/1.0\r\n\r\n' % path
    s.send(request.encode())

    buf = []
    callback = lambda: readable(s, buf)
    selector.register(s.fileno(), EVENT_READ, callback)

def readable(s, buf):
    global n_jobs
    selector.unregister(s.fileno())
    chunk = s.recv(1000)
    if chunk:
        buf.append(chunk)
        callback = lambda: readable(s, buf)
        selector.register(s.fileno(), EVENT_READ, callback)
    else:
        body = b''.join(buf).decode()
        print(body.split('\n')[0])
        n_jobs -= 1


@timeit
def exec():
    get('/delay/1')
    get('/delay/1')

    # This represents the event loop
    while n_jobs:
        events = selector.select()
        for key, _ in events:
            callback = key.data
            callback()

exec()


