""" Bubble Sort
This is called a naive approach.
Called bubble because in each iteration of the algorithm, the largest element of the array
will bubble on up to the top
Efficieny:
    Space: O(1)
    Time: O(n^2)

"""