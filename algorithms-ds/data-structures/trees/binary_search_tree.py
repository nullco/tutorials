"""
A binary search tree is a binary tree in which the children nodes at left are
always lower than the node value, and the rigth nodes are always greather than
the node value.

Efficiency:
    Searching: O(log(n)), Worst case O(n) when unbalanced
    Inserting: O(log(n)), Worst case O(n) when unbalanced
"""

from binary_tree import Node, BinaryTree

class BST(BinaryTree):

    def __init__(self, root):
        self.root = Node(root)

    def insert(self, new_val):
        """ Inserts a new node into the binary search tree

        It will start checking the root, and start going down the tree path
        that corresponds to storing the new node
        """
        self._insert_helper(self.root, new_val)

    def _insert_helper(self, node, new_val):
        if node.value < new_val:
            if node.right:
                self._insert_helper(node.right, new_val)
            else:
                node.right = Node(new_val)
        elif node.value > new_val:
            if node.left:
                self._insert_helper(node.left, new_val)
            else:
                node.left = Node(new_val)

    def search(self, find_val):
        """ searches for an existing value in the binary search tree

        It will start checking the root, and start going down the tree path
        that corresponds to the value position in the tree.
        """
        return self._search_helper(self.root, find_val)

    def _search_helper(self, node, find_val):
        if node:
            if node.value == find_val:
                return True
            elif node.value < find_val:
                return self._search_helper(node.right, find_val)
            else:
                return self._search_helper(node.left, find_val)
        return False


