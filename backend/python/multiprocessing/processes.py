"""
Manual creation of process instances to process data concurrently
"""
import time
import os
from multiprocessing import Process

start = time.perf_counter()

def do_something(seconds):
    pid = os.getpid()
    print(f'{pid}: Sleeping {seconds} second(s)...')
    # sleep is not CPU related
    time.sleep(seconds)
    print(f'{pid}: Done sleeping...')

processes = []

for _ in range(10):
    p = Process(target=do_something)
    p.start()
    processes.append(p)

for p in processes:
    # blocks main process until p finishes
    p.join()

finish = time.perf_counter()

print(f'Finished in {round(finish - start, 2)} second(s)')