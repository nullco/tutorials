""" Integer Difference
Write a function that accepts an array of random integers and an integer n. 
Determine the number of times where two integers in the array have the difference of n.

f(4, [1, 1, 5, 6, 9, 16, 27]) -> 3 (Due to 2x [1, 5], and [5, 9])
f(2, [1, 1, 3, 3]) -> 4 (Due to 4x [1, 3])
"""

class Naive:

    @staticmethod
    def integer_difference(array, n):
        """
        Efficiency:
            Time: O(n ^ 2)
            Space: O(1)
        """
        times = 0
        for i in range(len(array)):
            for j in range(i + 1, len(array)):
                if abs(array[i] - array[j]) == n:
                    times += 1
        return times


class Complement:
    """ Complement implementation

    This implementation uses complements of numbers that make the equation c - num = n
    The complement is just the number needed to satisfy the equation c - num = n
    and is defined as c = num + n

    This allows having to iterate over the array one single time
    """

    @staticmethod
    def integer_difference(array, n):
        """
        Efficiency:
            Time: O(n)
            Space: O(n)
        """
        times = 0
        occurences = {}
        # We iterate over the array just once
        for num in array:
            # we count the occurrences of numbers that had this one as complement
            if num in occurences:
                times += occurences[num]
            # we calculate the complement of this number for future lookups
            complement = num + n
            if complement not in occurences:
                occurences[complement] = 1
            else:
                occurences[complement] += 1
        return times
             
