import time

def timeit(func):

    def wrapper(*args, **kwargs):
        start = time.time()
        func(*args, **kwargs)
        print(' %s took %.2f seconds' % (func.__name__, (time.time() - start)))

    return wrapper