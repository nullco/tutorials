import socket
from timeit import timeit

def get(path):
    s = socket.socket()
    s.connect(('httpbin.org', 80))
    request = 'GET %s HTTP/1.0\r\n\r\n' % path
    s.send(request.encode())

    buf = []
    while True:
        chunk = s.recv(1000)
        if chunk:
            buf.append(chunk)
        else:
            body = b''.join(buf).decode()
            print(body.split('\n')[0])
            return

@timeit
def exec():
    get('/delay/1')
    get('/delay/1')

exec()


