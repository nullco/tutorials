""" Frog Jumps

There is a frog needing to cross a river, there are n stones in the river.
The frog can jump one or two stones in a row.

How many ways are there to cross the river?

"""

def frog_jumps(stones):
    if stones == 0:
        return 1
    elif stones == 1:
        return 2
    else:
        return frog_jumps(stones - 2) + frog_jumps(stones - 1)