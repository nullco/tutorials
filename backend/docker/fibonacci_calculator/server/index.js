const keys = require('./keys');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.json());

const { Pool } = require('pg');
const pgClient = new Pool({
    user: keys.pgUser,
    host: keys.pgHost,
    database: keys.pgDatabase,
    password: keys.pgPassword,
    port: keys.pgPort
});

pgClient.on('error', () => {
    console.log("Lost PG Connection");
});

pgClient.on('connect', () => {
    console.log("Connected to PG");
    pgClient
        .query('CREATE TABLE IF NOT EXISTS values (number INT)')
        .catch((err) => console.log(err));
});

const redis = require('redis');
const redisClient = redis.createClient({
    host: keys.redisHost,
    port: keys.redisPort,
    retry_strategy: () => 1000
});

// Client with connection in publisher mode cannot send normal commands to redis
// That's why we need a separate client
const redisPublisher = redisClient.duplicate();

app.get('/', (req, res) => {
    res.send('hi');
});

app.get('/values/all', async (req, res) => {
    // Return all historical indexes computed so far
    const values = await pgClient.query('SELECT * FROM values');
    res.send(values.rows);
});

app.get('/values/current', (req, res) => {
    // Return all Fibs computed so far
    redisClient.hgetall('values', (err, values) => {
        res.send(values);
    });
});

app.post('/values', async (req, res) => {
    const index = req.body.index;

    if (parseInt(index) > 40) {
        return res.status(422).send('Index too high');
    }

    // Clear index
    redisClient.hset('values', index, 'Nothing yet!');
    // Send an insert message with the index to be computed 
    redisPublisher.publish('insert', index);
    // Insert the index being computed to postgres
    pgClient.query('INSERT INTO values(number) VALUES ($1)', [index]);

    res.send({working: true});
});

app.listen(5000, err => {
    console.log("Listening");
})