import time
from multiprocessing import Process, Value, Lock

# This code makes use of locks to modify shared values across processes
# The use of locks is explicit in this case meaning that a separate Lock value is created.
# The Value class comes with a get_lock method that returns a instance of a lock associated to the value
# The Value class is considered thread and process safe

def add_500(total, lock):
    for _ in range(100):
        time.sleep(0.01)
        with lock:
            # This operation requires a lock since this operation translates into a read and write operations
            total.value += 5


def sub_500(total, lock):
    for _ in range(100):
        time.sleep(0.01)
        with lock:
            total.value -= 5

if __name__ == "__main__":
    total = Value('i', 500)
    lock = Lock()
    add_process = Process(target=add_500, args=(total, lock))
    sub_process = Process(target=sub_500, args=(total, lock))

    add_process.start()
    sub_process.start()

    add_process.join()
    sub_process.join()

    print(total.value)
    


