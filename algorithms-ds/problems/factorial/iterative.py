# f(5) = 5 * 4 * 3 * 2 * 1

def factorial(n):
    total = 1
    for i in range(n):
        total *= i + 1
    return total