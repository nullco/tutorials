"""Find Odd Occurring Element

Given an integer array, one element occurs odd number of times and all others 
have even occurrences. Find the element with odd occurrences.

[1, 1, 2, 2, 3, 4, 4] should return 3
"""

class Sets:

    @staticmethod
    def odd_ocurring_element(array):
        """
        Efficiency:
            Time: O(n)
            Space: O(n)
        """
        odd = set()
        even = set()
        for num in array:
            if num in odd:
                odd.remove(num)
                even.add(num)
            elif num in even:
                even.remove(num)
                odd.add(num)
            else:
                odd.add(num)
        return odd.pop()

class XOR:

    @staticmethod
    def odd_ocurring_element(array):
        """
        Efficiency:
            Time: O(n)
            Space: O(1)
        """
        result = 0
        for num in array:
            result ^= num
        return result

