from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

# An engine represents the core interface to a DB through a dialect
# manages a connection pool to the database
# lazy connection (created when used for the first time)
engine = create_engine('sqlite:///:memory:')

# Here a session factory is created (bound to engine)
Session = sessionmaker(bind=engine)
# instantiate a session. This will retrieve a connection from the engine when first used.
session = Session()

# base class that allows us to specify mappings to db tables directly into classes
Base = declarative_base()

class User(Base):
    
    # required
    __tablename__ = 'users'

    # at least an id is required
    # all 'Columns' are replaced with special python accessors called descriptors.
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    fullname = Column(String)
    nickname = Column(String)

    def __repr__(self):
       return "<User(id='%s', name='%s', fullname='%s', nickname='%s')>" % (
            self.id, self.name, self.fullname, self.nickname
        )

class Address(Base):

    __tablename__ = 'addresses'

    id = Column(Integer, primary_key=True, autoincrement=True)
    email_address = Column(String, nullable=False)
    user_id = Column(Integer, ForeignKey('users.id'))

    # We define a many-to-one relationship this way.
    user = relationship("User", back_populates="addresses")

    def __repr__(self):
            return "<Address(email_address='%s')>" % self.email_address

User.addresses = relationship("Address", back_populates="user", cascade="all, delete-orphan")

# The declarative base creates schema automatically for us. it can bee seen through <Model>.__table__

# this generates our schema within the database.
Base.metadata.create_all(engine)

# lets persist a new user
# the default constructor accepts keyword arguments for initialization
ed = User(name='ed', fullname='Ed Jones', nickname='edsnickname')

# add the user to the session. Now the user is 'pending'
session.add(ed)

# if the user is queried from the same session, the pending user will be flushed to db
# and returned as result.
other = session.query(User).filter_by(name='ed').first()

print("other == ed:", other == ed) # this should be true. 
# Thanks to identity map, there will always be one instance mapped to a primary key
print("ed.id: ", ed.id)

#lets add serveral users at once to the session
session.add_all([
    User(name='wendy', fullname='Wendy Williams', nickname='windy'),
    User(name='mary', fullname='Mary Contrary', nickname='mary'),
    User(name='fred', fullname='Fred Flintstone', nickname='freddy')])

# session pays attention to changes in objects it manages
ed.nickname = 'eddie'
print("session.dirty:", session.dirty) # should show user

# the rest are new
print("sesion.new:", session.new) # should print the 3 new users without changes

# with a commit, all pending changes are flushed to DB and the transaction is commited.
# the connection is returned to the pool.
session.commit()

# the session gets a new connection and creates a new transaction when a new change is made
ed.nickname = 'edd'
# this time we rollback
session.rollback()
print("rollback: ed.nickname:", ed.nickname) # shold be eddie

# To query, we use the query method on the session object
for r in session.query(User).order_by(User.id):
    print("object result:", r)

# quey also supports individual object attributes for a results. Returned as a tuple
for name, fullname in session.query(User.name, User.fullname):
    print("tuple result: ", name, fullname)

# We can also filter by keyword arguments
for name, in session.query(User.name).filter_by(fullname='Ed Jones'):
    print(name)

# Or user filter, which supports more flexible statements
for name, in session.query(User.name).filter(User.fullname=='Ed Jones'):
    print(name)

# Each operation to query returns a new query object (builder pattern)
# several operations can be anidated to produce complex queries
for user in session.query(User).\
                    filter(User.name=='ed').\
                    filter(User.fullname=='Ed Jones'):
    print(user)


# lets add another user with some email addresses
jack = User(name='jack', fullname='Jack Bean', nickname='gjffdd')
jack.addresses = [
    Address(email_address='jack@google.com'),
    Address(email_address='j25@yahoo.com')
]

print(jack.addresses[0].user) # should be jack

#both the user and addresses are persisted because of cascading
session.add(jack)
session.commit()

# accessing addresses will perform a lazy load from the DB (default behavior)
print(jack.addresses)
