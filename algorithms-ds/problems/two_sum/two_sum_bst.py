"""
Given a Binary Search Tree and a target number, return true if there exist two 
elements in the BST such that their sum is equal to the given target.

Example 1:

Input: 
    5
   / \ 
  3   6
 / \   \ 
2   4   7

Target = 9

Output: True
"""

class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):

    def find_target(self, root, k):
        traversal = []
        self.traversal(root, traversal)
        print(traversal)
        
        i, j = 0, len(traversal) - 1
        while j > i:
            res = traversal[i] + traversal[j]
            if res < k:
                i += 1
            elif res > k:
                j -= 1
            else:
                return True
        return False
    
    def traversal(self, node, traversal):
        """ Collects values in order by performing an "in-order" traversal

        Returns a list with all the values of the nodes
        """
        if node == None:
            return
        else:
            self.traversal(node.left, traversal)
            traversal.append(node.val)
            self.traversal(node.right, traversal)
        