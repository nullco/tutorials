import unittest
from binary_tree import BinaryTree, Node

class TestBinaryTreeCase(unittest.TestCase):

    def setUp(self):
        # Set up tree
        tree = BinaryTree(1)
        tree.root.left = Node(2)
        tree.root.right = Node(3)
        tree.root.left.left = Node(4)
        tree.root.left.right = Node(5)
        self.tree = tree

    def test_search(self):
        self.assertTrue(self.tree.search(4))
        self.assertFalse(self.tree.search(6))


    def test_print_preorder(self):
        self.assertEqual(self.tree.print_tree(), '1-2-4-5-3')

    def test_print_inorder(self):
        self.assertEqual(self.tree.print_tree(order="inorder"), '4-2-5-1-3')

    def test_print_postorder(self):
        self.assertEqual(self.tree.print_tree(order="postorder"), '4-5-2-3-1')