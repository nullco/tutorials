# 1 1
# 1 2
# 2 3
# 3 5

def fibonacci(n):
    one, two = 1, 1
    for _ in range(2, n):
        new = two + one
        one = two
        two = new
    return two