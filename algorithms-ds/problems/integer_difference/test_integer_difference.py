import unittest
from integer_difference import Naive, Complement

class TestIntegerDifferenceCase(unittest.TestCase):

    def test_integer_difference(self):
        arr = [1, 1, 5, 6, 9, 16, 27]
        num = 4
        expected = 3
        self.assertEqual(expected, Naive.integer_difference(arr, num))
        self.assertEqual(expected, Complement.integer_difference(arr, num))
        arr = [1, 1, 3, 3]
        num = 2
        expected = 4
        self.assertEqual(expected, Naive.integer_difference(arr, num))
        self.assertEqual(expected, Complement.integer_difference(arr, num))