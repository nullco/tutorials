""" Quick Sort

This algorithm works the following:

Select randomly a pivot (by convention it is the last element of the array)
Move everything that is greather than the pivot to the right of the pivot
Move everything that is lower than the pivot to the left of the pivot
Repeat this process with the left side of the pivot and right side of the pivot recursively

[6, 1, 3, 2*] -> [3, 1, 2*, 6] -> [1, <2>, 3, 6] -> [1, <2>, 3, 6]

Eficiency:
    Time: Average - O(n * log(n)), Worst - O(n ^ 2)
    Space: O(1)

    The worst case scenario in time efficiency occurs when the pivots are already sorted
    meaning that there is no left and right sides around the pivot.

    [1, 2, 4, 5, 6*]
"""

def quicksort(array):
    swap_pivot(array, 0, len(array) - 1)
    return array

def swap_pivot(array, start, end):
    if start >= end:
        return
    pivot = end
    compare_to = start
    while compare_to < pivot:
        if array[compare_to] > array[pivot]:
            array[pivot - 1], array[compare_to] = array[compare_to], array[pivot - 1]
            array[pivot], array[pivot - 1] = array[pivot - 1], array[pivot]
            pivot -= 1
        else:
            compare_to += 1
    swap_pivot(array, start, pivot - 1)
    swap_pivot(array, pivot + 1, end)