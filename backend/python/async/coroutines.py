# What is needed for coroutines:
# - future
# - generators
# - task

import socket
from timeit import timeit
from selectors import DefaultSelector, EVENT_WRITE, EVENT_READ

selector = DefaultSelector()
n_jobs = 0

class Future:
    """ Acts as a callbacks registry, when resolved executes each of the
    callbacks registered
    """
    def __init__(self):
        self.callbacks = []

    def resolve(self):
        for cb in self.callbacks:
            cb()

class Task:
    """Acts as a wrapper that handles the execution of a coroutine
    """

    def __init__(self, coro):
        self.coro = coro
        #execute the first step of the coroutine immediately
        self.step()

    def step(self):
        try:
            # each step generates a future and executes the code until the future
            future = next(self.coro)
        except StopIteration:
            return
        # the future then register a next step so it can be further processed by the event loop
        future.callbacks.append(self.step)

def get(path):
    """ This is our generator of futures
    """
    global n_jobs
    n_jobs += 1
    s = socket.socket()
    s.setblocking(False)
    try:
        # this raises an exception since connect does not return immediately
        s.connect(('httpbin.org', 80))
    except BlockingIOError:
        pass

    f = Future()
    selector.register(s.fileno(), EVENT_WRITE, f)
    yield f

    selector.unregister(s.fileno())
    request = 'GET %s HTTP/1.0\r\n\r\n' % path
    s.send(request.encode())

    buf = []

    while True:
        f = Future()
        selector.register(s.fileno(), EVENT_READ, f)
        yield f
        selector.unregister(s.fileno())

        chunk = s.recv(1000)
        if chunk:
            buf.append(chunk)
        else:
            body = b''.join(buf).decode()
            print(body.split('\n')[0])
            n_jobs -= 1
            return


@timeit
def exec():
    Task(get('/delay/1'))
    Task(get('/delay/1'))

    # This represents the event loop
    while n_jobs:
        events = selector.select()
        for key, _ in events:
            future = key.data
            future.resolve()

exec()


