"""
A binary tree is a tree that has nodes with left and child nodes

Traversal has two different ways:
Depth First Search (DFS) and  Breath First Search (BFS)

DFS and BFS can take several shapes

DFS:
    Pre-order traversal: Check the node, and then check the left and right children
    In-order traversal:
    Post-order traversal:
"""


class Node(object):

    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None


class BinaryTree(object):
    def __init__(self, root):
        self.root = Node(root)

    def search(self, find_val):
        """Return True if the value
        is in the tree, return
        False otherwise."""
        return self.preorder_search(self.root, find_val)

    def print_tree(self, order='preorder'):
        """Print out all tree nodes
        as they are visited in
        a pre-order traversal."""
        if order == 'inorder':
            return self.inorder_print(self.root, '')
        elif order == 'postorder':
            return self.postorder_print(self.root, '')
        else:
            return self.preorder_print(self.root, '')

    def preorder_search(self, start, find_val):
        if start is None:
            return False
        elif start.value == find_val:
            return True
        else:
            return self.preorder_search(start.left, find_val) or self.preorder_search(start.right, find_val)

    def preorder_print(self, start, traversal):
        if start is not None:
            traversal += '-' + str(start.value) if traversal else str(start.value)
            traversal = self.preorder_print(start.left, traversal)
            traversal = self.preorder_print(start.right, traversal)
        return traversal

    def inorder_print(self, start, traversal):
        if start is not None:
            traversal = self.inorder_print(start.left, traversal)
            traversal += '-' + str(start.value) if traversal else str(start.value)
            traversal = self.inorder_print(start.right, traversal)
        return traversal

    def postorder_print(self, start, traversal):
        if start is not None:
            traversal = self.postorder_print(start.left, traversal)
            traversal = self.postorder_print(start.right, traversal)
            traversal += '-' + str(start.value) if traversal else str(start.value)
        return traversal



