import unittest
import recursive
import iterative

class TestFactorialCase(unittest.TestCase):

    def test_recursive_factorial(self):
        self.assertEqual(recursive.factorial(5), 5 * 4 * 3 * 2 * 1)

    def test_iterateive_factorial(self):
        self.assertEqual(iterative.factorial(5), 5 * 4 * 3 * 2 * 1)
