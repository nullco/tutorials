import unittest
from binary_search_tree import BST, Node

class TestBinaryTreeCase(unittest.TestCase):

    def setUp(self):
        # Set up tree
        tree = BST(4)
        # Insert elements
        tree.insert(2)
        tree.insert(1)
        tree.insert(3)
        tree.insert(5)
        self.tree = tree

    def test_search(self):
        # Test search
        # Should be True
        self.assertTrue(self.tree.search(4))
        # Should be False
        self.assertFalse(self.tree.search(6))